const { authenticate } = require('@feathersjs/authentication').hooks

const proccessPosts = require('../../hooks/proccess-posts')

module.exports = {
    before: {
        all: [authenticate('jwt')],
        find: [],
        get: [],
        create: [proccessPosts()],
        update: [proccessPosts()],
        patch: [],
        remove: []
    },

    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
}
