const passwordValidator = require('password-validator')

module.exports = function(options = {}) {
    return async context => {
        const { data } = context
        const { email, password, name, phone } = data
        const schema = new passwordValidator()

        schema
            .is()
            .min(8)
            .is()
            .max(100)
            .has()
            .digits()
            .is()
            .not()
            .oneOf(['password', 'Password123', email])

        const validate = schema.validate(password, { list: true })

        if (!email) {
            throw new Error('Needs a email')
        }

        if (!name) {
            throw new Error('Needs a name')
        }

        if (!phone) {
            throw new Error('Needs a phone')
        }

        if (!password) {
            throw new Error('Needs a password')
        }

        if (validate.length > 0) {
            throw new Error(validate)
        }

        const checkExisting = await context.app
            .service('users')
            .find({ query: { email: email, $limit: 1 } })

        if (checkExisting.total > 0) {
            throw new Error('Email Exists')
        }

        // Override the original data (so that people can't submit additional stuff)
        context.data = {
            email,
            password,
            phone,
            name,
            createdAt: new Date().getTime()
        }

        // Best practise, hooks should always return the context
        return context
    }
}
