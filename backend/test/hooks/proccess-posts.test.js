const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const proccessPosts = require('../../src/hooks/proccess-posts');

describe('\'proccess-posts\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      get(id) {
        return Promise.resolve({ id });
      }
    });

    app.service('dummy').hooks({
      before: proccessPosts()
    });
  });

  it('runs the hook', () => {
    return app.service('dummy').get('test').then(result => {
      assert.deepEqual(result, { id: 'test' });
    });
  });
});
