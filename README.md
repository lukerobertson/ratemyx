TO DO:

front end

* Register page
* home page
* user account page

back end

* Posts API
* account details API

# Rate My XY

> Rate and share anything

Share your favourite media and see what people think of it.

### Prerequisites

Install Parcel as a global dependency:

```shell
npm install -g parcel-bundler
```

## Installing / Getting started

Get the DB running:

```shell
npm i -g rethinkdb
```

```shell
rethinkdb
```

Open a new terminal, then get the server running:

```shell
cd backend && npm start
```

Open a new terminal, then start the frontend:

```shell
cd ../frontend && npm install
```

```shell
npm start
```

This runs the project using Parcel to bundle everything.

## Developing

### User Interface

Use https://react.semantic-ui.com/ for all UI components.

### Built With

React, Redux, Parcel, Feathers

## Tests

Run all tests:

```shell
npm run test
```

Run all tests in watch mode:

```shell
npm run test:watch
```

### Help writing tests

https://github.com/reactjs/redux/blob/master/docs/recipes/WritingTests.md
