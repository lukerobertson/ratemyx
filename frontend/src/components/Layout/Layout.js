import React, { Component } from "react";
import injectSheet from "react-jss";
import { withRouter } from "react-router-dom";

import TopNav from "components/TopNav/TopNav";
import styles from "./LayoutStyles";

const Layout = ({ classes, history, children }) => (
  <div className={classes.container}>
    <TopNav history={history} />
    <div className={classes.mainContainer}>{children}</div>
  </div>
);

export default injectSheet(styles)(withRouter(Layout));
