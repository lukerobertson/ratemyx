const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    background: "#e0d9ca57",
    height: "100%"
  },
  mainContainer: {
    flex: "1",
    padding: theme.size.default,
    display: "flex"
  },
  "@global": {
    body: {
      height: "100%"
    },
    "#root": {
      height: "100%"
    },
    a: {}
  }
});

export default styles;
