import React, { Component } from "react";
import hoc from "utils/hoc";
import Presenter from "./AccountPM";
import styles from "./AccountStyles";

const Account = ({ pm }) => (
  <div className="layout-container">Account page</div>
);

export default hoc(Presenter, Account, styles);
