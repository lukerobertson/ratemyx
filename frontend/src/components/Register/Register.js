import React, { Component } from "react";
import { Form, Input, Icon, Checkbox, Button } from "antd";

import hoc from "utils/hoc";
import Presenter from "./RegisterPM";
import styles from "./RegisterStyles";

const FormItem = Form.Item;

const Register = ({ classes, children, pm, form }) => (
  <div className={classes.container}>
    <Form onSubmit={pm.handleSubmit} className={classes.form}>
      <FormItem {...pm.formItemLayout} label="E-mail">
        {form.getFieldDecorator("email", {
          rules: [
            {
              type: "email",
              message: "The input is not valid E-mail!"
            },
            {
              required: true,
              message: "Please input your E-mail!"
            }
          ]
        })(<Input autoComplete="email" />)}
      </FormItem>
      <FormItem {...pm.formItemLayout} label="Password">
        {form.getFieldDecorator("password", {
          rules: [
            {
              required: true,
              message: "Please input your password!"
            },
            {
              //pattern: "((?=.*d)(?=.*[a-z])(?=.*[A-Z]).{6,20})$",
              message:
                "Must contain a lower & upper case character, digit and be 6-20 in length."
            },
            {
              validator: pm.validateToNextPassword
            }
          ]
        })(<Input type="password" autoComplete="password" />)}
      </FormItem>
      <FormItem {...pm.formItemLayout} label="Confirm Password">
        {form.getFieldDecorator("confirm", {
          rules: [
            {
              required: true,
              message: "Please confirm your password!"
            },
            {
              validator: pm.compareToFirstPassword
            }
          ]
        })(
          <Input
            type="password"
            onBlur={pm.handleConfirmBlur}
            autoComplete="password"
          />
        )}
      </FormItem>
      <FormItem {...pm.formItemLayout} label={<span>Name&nbsp;</span>}>
        {form.getFieldDecorator("name", {
          rules: [
            {
              required: true,
              message: "Please input your name!",
              whitespace: true
            }
          ]
        })(<Input autoComplete="name" />)}
      </FormItem>
      <FormItem {...pm.formItemLayout} label="Phone Number">
        {form.getFieldDecorator("phone", {
          rules: [
            { required: true, message: "Please input your phone number!" }
          ]
        })(<Input autoComplete="tel-national" />)}
      </FormItem>

      <FormItem {...pm.tailFormItemLayout}>
        {form.getFieldDecorator("agreement", {
          valuePropName: "checked",
          rules: [
            {
              required: true,
              message: "Please confirm"
            }
          ]
        })(
          <Checkbox>
            I have read the <a href="">agreement</a>
          </Checkbox>
        )}
      </FormItem>
      <FormItem {...pm.tailFormItemLayout}>
        <Button type="primary" htmlType="submit">
          Register
        </Button>
      </FormItem>
    </Form>
  </div>
);

export default Form.create()(hoc(Presenter, Register, styles));
