import { observable, action } from "mobx";
import { message } from "antd";
import rest from "utils/rest";

class RegisterPM {
  @observable confirmDirty = false;

  constructor(props) {
    this.props = props;

    this.formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    this.tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };
  }

  @action
  handleConfirmBlur = e => {
    const value = e.target.value;
    this.confirmDirty = this.confirmDirty || !!value;
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.request(values);
      } else {
        message.error("Please double check all the fields and try again");
      }
    });
  };

  request = async body => {
    const response = await rest.register(
      JSON.stringify({
        email: body.email,
        password: body.password,
        phone: body.phone,
        name: body.name,
        strategy: "local"
      })
    );

    if (response.error) {
      message.error(`Error: ${response.message}`);
    } else {
      message.success(`Welcome ${body.email}, please login`);
      this.props.history.push("/login");
    }
  };
}

export default RegisterPM;
