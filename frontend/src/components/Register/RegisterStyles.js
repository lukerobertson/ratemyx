const styles = {
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flex: "1"
  },
  form: {
    width: "600px"
  }
};

export default styles;
