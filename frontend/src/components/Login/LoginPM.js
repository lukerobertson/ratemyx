import { message } from "antd";
import userStore from "/stores/userStore";
import rest from "utils/rest";

class RegisterPM {
  constructor(props) {
    this.props = props;
  }

  request = async (email, password) => {
    const response = await rest.login(
      JSON.stringify({
        email,
        password,
        strategy: "local"
      })
    );

    if (response.error) {
      message.error("Invalid login, please try again");
    } else {
      userStore.updateUserStore("token", response.accessToken);
      userStore.updateUserStore("email", email);
      message.success(`Welcome ${email}`);
      this.props.history.push("/");
    }
  };

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.request(values.email, values.password);
      } else {
        message.error("Invalid login, please try again");
      }
    });
  };
}

export default RegisterPM;
