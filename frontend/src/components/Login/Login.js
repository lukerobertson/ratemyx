import React, { Component } from "react";
import { Form, Icon, Input, Button, Checkbox } from "antd";

import hoc from "utils/hoc";
import Presenter from "./LoginPM";
import styles from "./LoginStyles";

const FormItem = Form.Item;

const Login = ({ classes, pm, form }) => (
  <div className={classes.container}>
    <Form onSubmit={e => pm.handleSubmit(e, form)} className={classes.form}>
      <FormItem>
        {form.getFieldDecorator("email", {
          rules: [
            {
              type: "email",
              message: "The input is not valid E-mail!"
            },
            {
              required: true,
              message: "Please input your E-mail!"
            }
          ]
        })(
          <Input
            prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
            placeholder="Email"
            autoComplete="email"
          />
        )}
      </FormItem>

      <FormItem>
        {form.getFieldDecorator("password", {
          rules: [{ required: true, message: "Please input your Password!" }]
        })(
          <Input
            prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
            type="password"
            placeholder="Password"
            autoComplete="password"
          />
        )}
      </FormItem>

      <FormItem>
        {form.getFieldDecorator("remember", {
          valuePropName: "checked",
          initialValue: true
        })(<Checkbox>Remember me</Checkbox>)}
        <a className={classes.forgot} href="">
          Forgot password
        </a>
        <Button type="primary" htmlType="submit" className={classes.button}>
          Log in
        </Button>
        Or <a href="">register now!</a>
      </FormItem>
    </Form>
  </div>
);

export default Form.create()(hoc(Presenter, Login, styles));
