const styles = {
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flex: "1"
  },
  form: {
    maxWidth: "300px"
  },
  forgot: {
    float: "right"
  },
  button: {
    width: "100%"
  }
};

export default styles;
