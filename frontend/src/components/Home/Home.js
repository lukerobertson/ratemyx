import React, { Component } from "react";
import hoc from "utils/hoc";
import Presenter from "./HomePM";
import styles from "./HomeStyles";

const Home = ({ pm }) => <div className="layout-container">home page</div>;

export default hoc(Presenter, Home, styles);
