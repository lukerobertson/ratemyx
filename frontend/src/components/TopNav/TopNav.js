import React from "react";
import { Menu, Icon } from "antd";

import hoc from "utils/hoc";
import userStore from "/stores/userStore";
import Presenter from "./TopNavPM";
import styles from "./TopNavStyles";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const TopNav = ({ pm, classes, children }) => (
  <Menu onClick={pm.navigate} selectedKeys={[pm.activeKey]} mode="horizontal">
    <Menu.Item key="/">
      <Icon type="home" />Home
    </Menu.Item>
    {!userStore.token && (
      <Menu.Item key="/login" style={{ float: "right" }}>
        <Icon type="unlock" />Login
      </Menu.Item>
    )}
    {!userStore.token && (
      <Menu.Item key="/register" style={{ float: "right" }}>
        <Icon type="user-add" />Register
      </Menu.Item>
    )}
    {userStore.token && (
      <Menu.Item key="/logout" style={{ float: "right" }}>
        <Icon type="lock" />
        Logout
      </Menu.Item>
    )}
    {userStore.token && (
      <Menu.Item key="/account" style={{ float: "right" }}>
        <Icon type="user" />
        {userStore.email}
      </Menu.Item>
    )}
  </Menu>
);

export default hoc(Presenter, TopNav, styles);
