import { observable, action } from "mobx";
import userStore from "stores/userStore";

class TopNavPM {
  @observable activeKey = this.props.history.location.pathname;

  @action
  navigate = item => {
    if (item.key === "/logout") {
      this.props.history.push("/");
      userStore.updateUserStore("token", null);
      userStore.updateUserStore("email", "");
      this.activeKey = "/";
    } else {
      this.props.history.push(item.key);
      this.activeKey = item.key;
    }
  };

  constructor(props) {
    this.props = props;
  }
}

export default TopNavPM;
