import React, { Component } from "react";
import { observer } from "mobx-react";
import injectSheet from "react-jss";

const presenter = (Presenter, View, styles) => {
  if (Presenter && View) {
    const ViewComp = observer(View);
    class WrappedView extends Component {
      static View = View;
      constructor(props) {
        super(props);
        const prps = Object.assign({}, View.defaultProps, props);
        this.pm = props.pm || new Presenter(prps, this);
        this.pm._hoc = !props.pm;
        this.view = View;
      }

      componentWillMount() {
        this.pm.componentWillMount && this.pm.componentWillMount();
      }

      componentWillReceiveProps(next) {
        this.pm.componentWillReceiveProps &&
          this.pm.componentWillReceiveProps(next);
      }

      componentWillUnmount() {
        this.pm.componentWillUnmount && this.pm.componentWillUnmount();
      }

      render() {
        return <ViewComp {...this.props} pm={this.pm} />;
      }
    }

    if (styles) {
      return injectSheet(styles)(WrappedView);
    }

    return WrappedView;
  }
  return componentClass => presenter(Presenter, componentClass);
};

export default presenter;
