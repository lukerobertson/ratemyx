import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "react-jss";

import Layout from "components/Layout/Layout";
import Home from "components/Home/Home";
import Login from "components/Login/Login";
import Register from "components/Register/Register";
import Account from "components/Account/Account";

import theme from "styles";

const router = () => (
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <Layout>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/account" component={Account} />

          <Route path="/" component={Home} />
        </Switch>
      </Layout>
    </ThemeProvider>
  </BrowserRouter>
);

export default router;
