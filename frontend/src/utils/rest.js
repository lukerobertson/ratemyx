import userStore from "/stores/userStore";
import { message } from "antd";

class Rest {
  fetch = async (url, method, body) => {
    try {
      const response = await fetch(`http://localhost:3030/${url}`, {
        method,
        headers: {
          "content-type": "application/json",
          Authorization: method == "GET" ? userStore.token : ""
        },
        body
      });
      const jsonData = await response.json();

      if (
        response.status === 404 ||
        response.status === 401 ||
        response.status === 500
      ) {
        return {
          error: true,
          message: jsonData.message
        };
      }

      return jsonData;
    } catch (e) {
      console.log("err", e);
      message.error("Uknown error");
    }
  };

  login = body => this.fetch("authentication", "POST", body);
  register = body => this.fetch("users", "POST", body);
  getUser = () => this.fetch(`users/${userStore.userId}`, "GET");
}

const rest = new Rest();

export default rest;
