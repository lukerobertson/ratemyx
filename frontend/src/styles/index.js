import colors from "./colors";
import size from "./size";

const theme = {
  colors: {
    ...colors
  },
  size: {
    ...size
  }
};

export default theme;
