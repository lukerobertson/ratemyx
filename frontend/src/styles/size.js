const size = {
  small: "10px",
  default: "20px",
  large: "40px"
};

export default size;
