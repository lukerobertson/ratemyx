import { observable, action, computed } from "mobx";
import decode from "jwt-decode";

class UserStore {
  @observable token = null;
  @observable email = "";

  @action
  updateUserStore = (target, val) => {
    this[target] = val;
  };

  @computed
  get userId() {
    console.log("555", this.token);
    try {
      return decode(this.token).userId;
    } catch (e) {
      return e;
    }
  }
}

const userStore = new UserStore();

export default userStore;
