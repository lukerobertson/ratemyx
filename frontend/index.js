import React from "react";
import ReactDOM from "react-dom";
import Router from "utils/router";
import "babel-polyfill";

ReactDOM.render(<Router />, document.getElementById("root"));
